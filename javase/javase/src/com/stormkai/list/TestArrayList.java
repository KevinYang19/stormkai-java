package com.stormkai.list;

import java.util.Arrays;
/**
 * 自定义一个ArrayList
 * @author Admin
 *
 */
public class TestArrayList {
	
	private Object[] elementData;
	private int size;
	
	private static final int DEFAULT_CAPACITY = 10;
	
	//无参构造器，可以定义ArrayList初始化的大小为10
	public TestArrayList() {
		elementData = new Object[DEFAULT_CAPACITY];
	}
	
	//自定义创建ArrayList时候初始化数组的大小
	public TestArrayList(int capacity) {
		elementData = new Object[capacity];
	}
	
	
	public void add(Object obj) {
		elementData[size++] = obj;
	}
	
	



	@Override
	public String toString() {
		return "TestArrayList [elementData=" + Arrays.toString(elementData) + ", size=" + size + "]";
	}

	public static void main(String[] args) {
		TestArrayList tList = new TestArrayList(20);
		tList.add("aa");
		tList.add("bb");
		
		System.out.println(tList.toString());

	}

}
