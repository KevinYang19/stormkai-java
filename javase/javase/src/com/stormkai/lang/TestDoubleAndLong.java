package com.stormkai.lang;

import java.math.BigDecimal;
import java.util.UUID;

public class TestDoubleAndLong {

	public static void main(String[] args) {
		double a = 7.56;
		long b = (long) a;
		System.out.println("b="+b);
		System.out.println("a四舍五入后="+Math.round(a));
		
		/**
		 * 2^63
		 */
		BigDecimal bg = new BigDecimal(2);
		System.out.println(bg.pow(63));
		
		System.out.println(UUID.randomUUID());

	}

}
