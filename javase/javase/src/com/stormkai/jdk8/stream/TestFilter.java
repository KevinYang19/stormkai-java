package com.stormkai.jdk8.stream;

import java.util.stream.Stream;

public class TestFilter {

	public static void main(String[] args) {
		//创建一个Stream流
		Stream<String> stream = Stream.of("Lily","Helena","Lucy","Simon");
		
		//对Stream流中的元素进行过滤，只要以“L”开头的
		Stream<String> stream1 = stream.filter(str -> str.startsWith("L"));
		
		//遍历stream1流
		stream1.forEach(name -> System.out.println(name));
		
		/**
		 * Stream流属于管道流，只能被消息（使用）一次
		 * 第一个Stream流调用完毕方法，数据就会流转到下一个Stream上
		 * 而这时第一个Stream流已经使用完毕，就会关闭了
		 * 所以第一个Stream流就不能再调用方法了
		 * java.lang.IllegalStateException: stream has already been operated upon or closed
		 */
		//stream.forEach(name -> System.out.println(name));

	}

}
