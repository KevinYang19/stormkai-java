package com.stormkai.jdk8.stream;

import java.util.stream.Stream;

public class TestLimit {

	public static void main(String[] args) {
		String[] arr = {"aaa","bbb","ccc","ddd","eee"};
		Stream<String> stream = Stream.of(arr);
		
		//使用limit对Stream流中的元素进行截取，只要前3个元素
		Stream<String> stream2 = stream.limit(3);
		
		stream2.forEach(str -> System.out.println(str));

	}

}
