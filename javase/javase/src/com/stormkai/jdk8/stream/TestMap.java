package com.stormkai.jdk8.stream;

import java.util.stream.Stream;

public class TestMap {
	
	public static void main(String[] args) {
		//获取一个String类型的Stream流
		Stream<String> stream = Stream.of("1","2","3","4");
		
		//使用map方法，将字符串类型的整数，转换（映射）为Integer类型的整数
		Stream<Integer> stream1 = stream.map((String str) -> {
			return Integer.parseInt(str);
		});
		
		//遍历Stream流
		stream1.forEach(i -> System.out.println(i));
	}
	
	

}
