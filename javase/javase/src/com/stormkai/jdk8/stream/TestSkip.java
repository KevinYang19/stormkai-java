package com.stormkai.jdk8.stream;

import java.util.stream.Stream;

public class TestSkip {
	
	public static void main(String[] args) {
		String[] arr = {"aaa","bbb","ccc","ddd","eee"};
		Stream<String> stream = Stream.of(arr);
		
		//使用skip方法跳过前3个元素
		Stream<String> stream2 = stream.skip(3);
		
		stream2.forEach(str -> System.out.println(str));

	}

}
