package com.stormkai.jdk8.stream;

import java.util.stream.Stream;
/**
 * forEach方法用来遍历流中的数据
 * 是一个终结方法，遍历之后就不能继续使用Stream流中的其他方法
 * @author Admin
 *
 */
public class TestForEach {

	public static void main(String[] args) {
		//获取一个Stream流
		Stream<String> stream = Stream.of("Lily","Helena","Lucy","Simon");
		
		//使用Stream流中的forEach对Stream流中的数据进行遍历
		/*stream.forEach((String str) -> {
			System.out.println(str);
		});*/
		
		stream.forEach(str -> System.out.println(str));
	}

}
