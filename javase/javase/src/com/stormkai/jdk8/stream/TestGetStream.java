package com.stormkai.jdk8.stream;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

public class TestGetStream {

	public static void main(String[] args) {
		//把集合转换为Stream流
		List<String> list = new ArrayList<>();
		Stream<String> streamList = list.stream();
		
		Set<String> set = new HashSet<>();
		Stream<String> streamSet = set.stream();
		
		Map<String, String> map = new HashMap<>();
		//获取键，存储到一个Set集合中
		Set<String> keySet = map.keySet();
		Stream<String> streamKeySet = keySet.stream();
		
		//获取值，存储到一个Collection集合中
		Collection<String> values = map.values();
		Stream<String> streamValues = values.stream();
		
		//获取键值对（键与值的映射关系 entrySet）
		Set<Map.Entry<String, String>> entries = map.entrySet();
		Stream<Map.Entry<String, String>> streamEntries = entries.stream();
		
		//将数组转换为Stream流
		Stream<Integer> stream = Stream.of(1,2,3,4,5);
		
		//可变参数可以传递数组
		Integer[] arr = {1,2,3,4,5};
		Stream<Integer> streamArr1 = Stream.of(arr);
		
		String[] arr1 = {"aaa","bbb","ccc"};
		Stream<String> streamArr2 = Stream.of(arr1);
		

	}

}
