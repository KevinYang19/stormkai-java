package com.stormkai.jdk8.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class TestCount {

	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(1,2,3,4,5,6,7);
		Stream<Integer> stream = list.stream();
		
		long count = stream.count();
		System.out.println(count);//7
		

	}

}
