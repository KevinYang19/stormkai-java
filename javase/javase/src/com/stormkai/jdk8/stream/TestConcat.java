package com.stormkai.jdk8.stream;

import java.util.stream.Stream;

public class TestConcat {

	public static void main(String[] args) {
		String[] arr = {"aaa","bbb","ccc","ddd","eee"};
		Stream<String> stream1 = Stream.of(arr);
		
		Stream<String> stream2 = Stream.of("Lily","Lucy","Helena","Simon");
		
		//把以上两个流组合为一个流
		Stream<String> stream = Stream.concat(stream1, stream2);
		
		//遍历concat流
		stream.forEach(str -> System.out.println(str));

	}

}
