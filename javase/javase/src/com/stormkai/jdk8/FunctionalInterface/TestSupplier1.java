package com.stormkai.jdk8.FunctionalInterface;

import java.util.function.Supplier;

/**
 * java.util.function.Supplier<T> 接口仅包含一个无参的方法：T get()。用来获取一个泛型参数指定类型的对象数据
 * Supplier<T>接口被称之为生产型接口，指定接口的泛型是什么类型，那么接口中的get方法就会产生什么类型的数据
 * @author yangkaid
 *
 */
public class TestSupplier1 {
	
	public static String getString(Supplier<String> sup) {
		return sup.get();
	}

	public static void main(String[] args) {
		String s = getString(() -> {
			return "你还，世界！";
		});
		
		System.out.println(s);
		
		//优化Lambda表达式
		String s1 = getString(() -> "Hello World!");
		System.out.println(s1);

	}

}
