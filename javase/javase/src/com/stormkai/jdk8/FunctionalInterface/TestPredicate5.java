package com.stormkai.jdk8.FunctionalInterface;

import java.util.ArrayList;
import java.util.function.Predicate;
/**
 * 集合信息筛选
 * 数组当中有多条“姓名+性别”的信息如下：
 * String[] arr = {"迪丽热巴，女","古力娜扎，女","马尔扎哈，男","赵丽颖，女"};
 * 请通过Predicate接口的拼装将符合要求的字符串筛选到集合ArrayList中，
 * 需同时满足两个条件：
 * 	1.必须为女生
 * 	2.姓名为4个字
 * @author Admin
 *
 */
public class TestPredicate5 {
	
	public static ArrayList<String> filter(String[] arr, Predicate<String> pre1, Predicate<String> pre2){
		ArrayList<String> list = new ArrayList<>();
		for (String s : arr) {
			boolean b = pre1.and(pre2).test(s);
			
			if(b) {
				list.add(s);
			}
		}
		
		return list;
	}

	public static void main(String[] args) {
		
		String[] array = {"迪丽热巴，女","古力娜扎，女","马尔扎哈，男","赵丽颖，女"};
		
		ArrayList<String> list = filter(array, (String s) -> {
			return s.split("，")[1].equals("女");
		}, (String s) -> {
			return s.split("，")[0].length()==4;
		});
		
		for(String s : list) {
			System.out.println(s);
		}

	}

}
