package com.stormkai.jdk8.FunctionalInterface;

import java.util.function.Consumer;

public class TestConsumer2 {
	
	public static void method(String s, Consumer<String> con1, Consumer<String> con2) {
		/*con1.accept(s);
		con2.accept(s);*/
		
		//使用andThen方法，把两个Consumer接口连接到一起，在消费数据
		con1.andThen(con2).accept(s);//con1连接con2,先执行con1消费数据,在执行con2消费数据
	}

	public static void main(String[] args) {
		method("Hello", (t) -> {
			//消费方式：把字符串转换成大写输出
			System.out.println(t.toUpperCase());
		}, (t) -> {
			//消费方式：把字符串转换成小写输出
			System.out.println(t.toLowerCase());
		});

	}

}
