package com.stormkai.jdk8.FunctionalInterface;

import java.util.function.Predicate;

/**
 * 逻辑表达式，可以连接多个判断的条件
 * &&: 与运算符，有false则false
 * ||: 或运算符，有true则true
 * !: 非（取反）运算符，非真则假，非假则真
 * 
 * 需求：判断一个字符串
 * 	1.判断字符串长度是否大于5
 * 	2.判断字符串中是否包含b
 * 两个条件必须同时满足，我们可以使用&&运算符连接两个条件
 * @author Admin
 *
 */
public class TestPredicate2 {
	
	public static boolean checkString(String s, Predicate<String> pre1, Predicate<String> pre2) {
		//return pre1.test(s) && pre2.test(s);
		return pre1.and(pre2).test(s);
	}
	
	public static void main(String[] args) {
		
		String s = "bcdeff";
		
		boolean b = checkString(s, (String str) -> {
			return str.length() > 5;
		}, (String str) -> {
			return str.contains("b");
		});
		
		System.out.println(b);//true
	}

}
