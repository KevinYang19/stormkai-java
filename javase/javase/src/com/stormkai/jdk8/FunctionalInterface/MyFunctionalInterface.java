package com.stormkai.jdk8.FunctionalInterface;

/**
 * @FunctionalInterface注解
 *  作用：可以检测接口是否是一个函数式接口
 *  是：编译成功
 *  否：编译失败（接口中没有抽象方法或者抽象方法的个数多余1个）
 * @author yangkaid
 *
 */
@FunctionalInterface
public interface MyFunctionalInterface {
	
	public abstract void method();

}
