package com.stormkai.jdk8.FunctionalInterface;

import java.util.function.Supplier;

/**
 * 求数组元素最大值
 * 	使用Supplier接口作为方法的参数类型，通过Lambda表达式求出int数组中最大值
 * @author yangkaid
 *
 */
public class TestSupplier2 {
	//定义一个方法，用于获取int类型数组中元素的最大值，方法的参数传递Supplier接口，泛型使用Integer
	public static int getMax(Supplier<Integer> sup) {
		return sup.get();
	}

	public static void main(String[] args) {
		int[] arr = {100,0,-32,87,98,22,-25,188};
		
		int maxValue = getMax(() -> {
			int max = arr[0];
			for (int i : arr) {
				if(i > max) {
					max = i;
				}
			}
			return max;
		});
		System.out.println("数组中元素的最大值是："+maxValue);

	}

}
