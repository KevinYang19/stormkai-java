package com.stormkai.jdk8.FunctionalInterface;

import java.util.function.Consumer;

/**
 * 字符串数组当中存有多条信息，请按照格式“姓名：xx。性别：xx。”的格式将信息打印出来。
 * 要求将打印姓名的动作作为第一个Consumer接口的Lambda实例，
 * 将打印性别的动作作为第二个Consumer接口的Lambda实例，
 * 将两个Consumer接口按照顺序“拼接”到一起。
 * @author yangkaid
 *
 */
public class TestConsumer3 {
	
	public static void printInfo(String[] arr, Consumer<String> con1, Consumer<String> con2) {
		//遍历字符串数组
		for (String str : arr) {
			con1.andThen(con2).accept(str);
		}
	}

	public static void main(String[] args) {
		//定义一个字符串类型的数组
		String[] arr = {"迪丽热巴，女","古力娜扎，女","马尔扎哈，男"};
		
		printInfo(arr, (str) -> {
			String name = str.split("，")[0];
			System.out.println("姓名："+name);
		}, (str) -> {
			String sex = str.split("，")[1];
			System.out.println("性别："+sex);
		});

	}
	

}
