package com.stormkai.jdk8.FunctionalInterface;

import java.util.Arrays;
import java.util.Comparator;

/**
 * 如果一个方法的返回值类型是一个函数式接口，那么就可以直接返回一个Lambda表达式
 * 需要通过一个方法获取一个java.util.Comparator接口类型的对象作为排序器时，就可以调该方法获取
 * @author yangkaid
 *
 */
public class TestComparator1 {
	
	public static Comparator<String> getComparator(){
		
		/*return new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				// 按照字符串的降序排序
				return o2.length() - o1.length();
			}
		};*/
		
		//方法的返回值类型是一个函数式接口，所以我们可以返回一个Lambda表达式
		/*return (String o1, String o2) -> {
			return o2.length() - o1.length();
		};*/
		
		//优化Lambda
		return (o1, o2) -> o2.length() - o1.length();
	}
	
	public static void main(String[] args) {
		String[] arr = {"aaa","b","cccccc","ddddddd"};
		
		System.out.println(Arrays.toString(arr));
		
		Arrays.sort(arr, getComparator());
		
		System.out.println(Arrays.toString(arr));
	}

}
