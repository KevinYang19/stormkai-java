package com.stormkai.jdk8.FunctionalInterface;

import java.util.function.Function;

/**
 * 将String类型转换为Integer类型
 * @author Admin
 *
 */
public class TestFunction {
	
	/**
	 * 定义一个方法
	 * 方法的参数传递一个字符串类型的整数
	 * 方法的参数传递一个Function接口，泛型使用<String, Integer>
	 * 使用Function接口中的方法apply,把字符串类型的整数，转换为Integer类型的整数
	 * @param s
	 * @param fun
	 */
	public static void change(String s, Function<String, Integer> fun) {
		Integer in = fun.apply(s);
		System.out.println(s);
	}

	public static void main(String[] args) {
		//定义一个字符串类型的整数
		String s = "1234";
		
		//调用change方法，传递字符串类型的整数和Lambda表达式
		change(s, (String str) -> {
			//把字符串类型的整数，转换为Integer类型的整数返回
			return Integer.parseInt(str);
		});
		
		//优化Lambda
		change(s, str -> Integer.parseInt(str));
		
		
		

	}

}
