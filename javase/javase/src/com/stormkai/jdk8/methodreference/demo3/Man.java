package com.stormkai.jdk8.methodreference.demo3;

/**
 * 通过Super引用成员方法
 * @author Admin
 *
 */
public class Man extends Human {
	//子类重写父类sayHello的方法
	@Override
	public void sayHello() {
		System.out.println("Hello,我是Man!");
	}
	
	//定义一个方法参数传递Greetable接口
	public void method(Greetable g) {
		g.greet();
	}
	
	public void show() {
		//调用method方法
		/*method(() -> {
			Human h = new Human();
			h.sayHello();
		});*/
		
		/*method(() -> {
			super.sayHello();
		});*/
		
		/*
		 * 使用super引用类的成员方法
		 * super是已经存在的
		 * 父类的成员方法sayHello也是已经存在的
		 * 所以我们可以直接使用super引用弗雷德成员方法
		 */
		method(super :: sayHello);
	}

	public static void main(String[] args) {
		
		new Man().show();

	}

}
