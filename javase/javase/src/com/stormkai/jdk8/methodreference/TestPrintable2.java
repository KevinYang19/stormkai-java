package com.stormkai.jdk8.methodreference;
/**
 * 通过对象名引用成员方法
 * 使用前提是对象名是已经存在的，成员方法也是已经存在
 * 就可以使用对象名来引用成员方法
 * @author Admin
 *
 */
public class TestPrintable2 {
	//定义一个方法，方法的参数传递Printable接口
	public static void printString(Printable p) {
		p.print("Hello World");
	}

	public static void main(String[] args) {
		//调用printString方法，方法的参数Printable是一个函数式接口，所以可以传递Lambda表达式
		printString((s) -> {
			//创建MethodRerObject对象
			MethodRerObject m = new MethodRerObject();
			//调用MethodRerObject对象中的成员方法printUpperCaseString，把字符串按照大写输出
			m.printUpperCaseString(s);
		});
		
		/*
		 * 使用方法引用优化Lambda
		 * 对象是已经存在的MethodRerObject
		 * 成员方法也是已经存在的printUpperCaseString
		 * 所以我们可以使用对象名引用成员方法
		 */
		//创建MethodRerObject对象
		MethodRerObject m = new MethodRerObject();
		printString(m :: printUpperCaseString);

	}

}
