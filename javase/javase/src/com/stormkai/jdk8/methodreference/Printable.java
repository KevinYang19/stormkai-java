package com.stormkai.jdk8.methodreference;

@FunctionalInterface
public interface Printable {
	
	void print(String s);

}
