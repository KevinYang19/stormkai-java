package com.stormkai.jdk8.lambda.demo1;
/**
 * 日志案例
 * 
 * 以下代码存在的性能浪费的问题
 * 调用showLog方法，传递的第二个参数是一个拼接后的字符串
 * 先把字符串拼接好，然后再调用showLog方法
 * showLog方法中如果传递的日志等级不是1级
 * 那么就不会输出拼接后的字符串
 * 所以感觉字符串白拼接了，存在了浪费
 * @author yangkaid
 *
 */
public class TestLogger {
	//定义一个根据日志的级别，显示日志信息的方法
	public static void showLog(int level, String msg) {
		//对日志等级进行判断，如果是1级别，那么输出日志信息
		if(level == 1) {
			System.out.println(msg);
		}
	}
	
	public static void main(String[] args) {
		//定义三个日志信息
		String msg1 = "Hello";
		String msg2 = "World";
		String msg3 = "Java";
		
		showLog(2, msg1+msg2+msg3);
	}

}
