package com.stormkai.jdk8.lambda.demo1;

/**
 * 使用Lambda优化日志案例
 * Lambda特点：延迟加载
 * Lambda的使用前提，必须存在函数式接口
 * @author yangkaid
 *
 */
public class TestLambdaLogger {
	//定义一个显示日志的方法，方法的参数传递日志的等级和MessageBuilder接口
	public static void showLog(int level, MessageBuilder mb) {
		//对日志的等级进行判断，如果是1级，则调用MessageBuilder接口中的builderMessage方法
		if(level == 1) {
			System.out.println(mb.builderMessage());
		}
	}

	public static void main(String[] args) {
		String msg1 = "Hello";
		String msg2 = "World";
		String msg3 = "Java";
		
		/*showLog(1, () -> {
			return msg1+msg2+msg3;
		});*/
		
		/**
		 * 使用Lambda表达式作为参数传递，仅仅是把参数传递到showLog方法中
		 * 只有满足条件，日志等级是1级
		 * 	才会调用接口MessageBuilder中的方法builderMessage
		 * 	才会进行字符串的拼接
		 *如果添加不满足，日志的等级不是1级
		 *	那么MessageBuilder接口中的方法builderMessage也不会执行
		 *	所以拼接字符串的代码也不会执行
		 *所以不存在性能的浪费
		 */
		showLog(2, () -> {
			System.out.println("不满足条件不执行");
			return msg1+msg2+msg3;
		});

	}

}
