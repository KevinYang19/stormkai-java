package com.stormkai.annotation;

import java.lang.annotation.Annotation;

public class TestUserAnnotation {

	public static void main(String[] args) {
		try {
			Class clazz = Class.forName("com.stormkai.annotation.User");
		
			//获得类的所有有效注解
			/*Annotation[] annotations = clazz.getAnnotations();
			for(Annotation a: annotations) {
				System.out.println("a="+a);
			}
			
			//获得类的指定的注解
			Table t = (Table) clazz.getAnnotation(Table.class);
			System.out.println("t.value()="+t.value());
			
			//获得类的属性的注解
			java.lang.reflect.Field f = clazz.getDeclaredField("name");
			Field field = f.getAnnotation(Field.class);
			System.out.println(field.columnName()+"---"+field.type()+"---"+field.length());
			
			Field field1;
			java.lang.reflect.Field[] ff = clazz.getDeclaredFields();
			for(int i=0; i<ff.length; i++) {
				ff[i].setAccessible(true);
				
				field1 = clazz.getDeclaredField(ff[i].getName()).getAnnotation(Field.class);
				System.out.println(field1.columnName()+"---"+field1.type()+"---"+field1.length());
				
			}*/
			
			//根据获得的表名，字段的信息，拼出DDL语句
			Annotation[] annotations = clazz.getAnnotations();
			Table t = (Table) clazz.getAnnotation(Table.class);
			StringBuilder sb = new StringBuilder();
			sb.append("create table "+t.value()+"( ");
			
			Field field;
			java.lang.reflect.Field[] ff = clazz.getDeclaredFields();
			for(int i=0; i<ff.length; i++) {
				ff[i].setAccessible(true);
				
				field = clazz.getDeclaredField(ff[i].getName()).getAnnotation(Field.class);
				System.out.println(field.columnName()+"---"+field.type()+"---"+field.length());
				sb.append(field.columnName()+" "+field.type()+"("+field.length()+"), ");
				
			}
			String str = sb.substring(0, sb.lastIndexOf(","));
			str+=" );";
			
			
			System.out.println(str);
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		

	}

}
