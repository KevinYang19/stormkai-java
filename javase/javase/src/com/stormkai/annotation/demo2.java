package com.stormkai.annotation;
/**
 * 测试自定义注解的使用
 * @author Admin
 *
 */
@Annotation01
public class demo2 {
	
	@Annotation01(name="zhangsan", id=1001, age=28, schools={"西安交通大学","西北工业大学"})
	public void test1() {
		
	}
	
	@Annotation02("aaa")
	public void test2() {
		
	}

}
