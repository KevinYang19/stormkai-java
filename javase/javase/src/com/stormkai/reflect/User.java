package com.stormkai.reflect;

public class User {
	
	private long id;
	private String name;
	private int age;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	public User() {
		System.out.println("User()构造器执行...");
	}
	public User(long id, String name, int age) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		System.out.println("User(long id, String name, int age)构造器执行...");
	}
	
	@Override
	public String toString() {
		return super.toString();
	}

}
