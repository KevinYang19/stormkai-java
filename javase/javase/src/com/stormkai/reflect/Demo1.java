package com.stormkai.reflect;

public class Demo1 {

	public static void main(String[] args) {
		String path = "com.stormkai.annotation.reflect.User";
		
		try {
			Class clazz = Class.forName(path);
			System.out.println(clazz.hashCode());//2018699554
			
			Class clazz2 = Class.forName(path);
			System.out.println(clazz2.hashCode());//2018699554
			
			Class strClazz = String.class;
			Class strClazz2 = path.getClass();
			System.out.println(strClazz==strClazz2);//true
			
			Class intClazz = int.class;
			
			int[] arr01 = new int[10];
			int[] arr02 = new int[30];
			System.out.println(arr01.getClass().hashCode());//1311053135
			System.out.println(arr02.getClass().hashCode());//1311053135
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

}
