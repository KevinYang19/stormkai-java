package com.stormkai.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
/**
 * 通过反射动态的操作构造器，方法，属性
 * @author yangkaid
 *
 */
public class Demo3 {
	
	public static void main(String[] args) {
		String path = "com.stormkai.reflect.User";
		
		try {
			Class<User> clazz = (Class<User>)Class.forName(path);
			
			//通过反射API调用构造方法，构造对象
			User user = clazz.newInstance(); //其实是调用了user的无参构造方法
			System.out.println(user);
			
			Constructor<User> c = clazz.getDeclaredConstructor(long.class, String.class, int.class);
			User userContructor = c.newInstance(1001L, "Kevin", 30);//调用了user的有参构造器
			System.out.println(userContructor.getName());
			
			//通过反射API调用普通方法
			User user1 = clazz.newInstance();
			Method method = clazz.getDeclaredMethod("setName", String.class);
			method.invoke(user1, "Kevin");
			System.out.println(user1.getName());
			
			//通过反射API操作属性
			User user2 = clazz.newInstance();
			Field f = clazz.getDeclaredField("name");
			f.setAccessible(true);//这个属性不需要做安全检查了，可以直接访问private的
			f.set(user2, "stormkai"); //通过反射直接写属性
			System.out.println(user2.getName()); //通过反射直接读属性的值
			System.out.println(f.get(user2));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
