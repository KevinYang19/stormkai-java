package com.stormkai.reflect.demo2;

public class ClassDemo1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//A的实例对象如何表示
		A a = new A();//a就表示出来了
		//A这个类也是一个实例对象，Class累的实例对象，如何表示呢
		//任何一个类都是Class的实例对象，这个实例对象有三种表示方式
		
		//第一种表示方式-->实际在告诉我们任何一个类都有一个隐含的静态成员变量
		Class c1 = A.class;
		
		//第二种表达方式 已经知道该类的对象通过getClass方法
		Class c2 = a.getClass();
		
		/*
		 * 官网c1,c2表示了Foo类的类类型（class type）
		 * 完事万物皆对象
		 * 类也是对象，是Class累的实例对象
		 * 这个对象我们成为该类的类类型
		 */
		System.out.println(c1 == c2);
		
		//第三种表达方式
		Class c3 = null;
		try {
			c3 = Class.forName("com.stormkai.reflect.demo2.A");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println(c2==c3);
		
		try {
			A aa = (A)c1.newInstance();//需要有无参数
			aa.print();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

	}

}

class A{
	void print(){
		System.out.println("hello A...");
	}
	
}
