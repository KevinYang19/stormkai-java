package com.stormkai.reflect.demo2;

import java.lang.reflect.Method;

public class MethodDemo1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// 要获取print(int,int)方法		1.要获取一个方法就是获取累的信息
		Aoo aoo1 = new Aoo();
		Class c = aoo1.getClass();
		/*
		 * 2.获取方法	名称和参数列表来决定
		 * getMethod获取的是public的方法
		 * getDeclaredMethod自己声明的方法
		 */
		try {
			//Method m = c.getMethod("print", new Class[]{int.class,int.class});
			Method m = c.getMethod("print", int.class, int.class);
			
			//方法的反射操作
			//aoo1.prin(10,20);方法的反射操作是用m对象来进行方法调用 和aoo1.print调用的效果完全相同
			//方法如果没有返回值返回null,有返回值返回具体的返回值
			//Object o = m.invoke(aoo1, new Object[]{10,20});
			Object o = m.invoke(aoo1, 10, 20);
			System.out.println("============================");
			//获取方法print(String,String)
			Method m1 = c.getMethod("print", String.class, String.class);
			//用方法进行反射操作
			//aoo1.print("hello","WORLD");
			o = m1.invoke(aoo1, "hello", "WORLD");
			
			//Method m2 = c.getMethod("print", new Class[]{});
			Method m2 = c.getMethod("print");
			//m2.invoke(aoo1, new Object[]{});
			m2.invoke(aoo1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

class Aoo{
	public void print(){
		System.out.println("helloworld");
	}
	public void print(int a, int b){
		System.out.println(a+b);
	}
	public void print(String a, String b){
		System.out.println(a.toUpperCase()+","+b.toLowerCase());
	}
}
