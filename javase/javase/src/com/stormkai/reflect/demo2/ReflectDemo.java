package com.stormkai.reflect.demo2;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ReflectDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//reflect("s");
		reflect(1);

	}
	
	/**
	 * 反射方法
	 * 用于实现
	 * 			obj 的类型是什么
	 * 			obj 有哪些属性
	 * 			obj 有哪些方法
	 * 			obj 有哪些构造器
	 * @param obj表示被反射的对象，被用于发现的对象
	 */
	public static void reflect(Object obj){
		//1.getClass()
		//	返回对象的类型，是Object类的方法
		Class cls = obj.getClass();
		System.out.println("类："+cls.getName());
		
		//2.getDeclaredFields()
		//	返回在类上声明的所有属性(字段)
		Field[] fields = cls.getDeclaredFields();
		System.out.println("属性：");
		for (Field field : fields) {
			System.out.println(
					field.getType()+":"+//属性类型
							field.getName());//属性名称
		}
		
		//3.getDeclaredMethods()
		//	返回在类上声明的所有方法
		Method[] methods = cls.getDeclaredMethods();
		System.out.println("方法：");
		for (Method method : methods) {
			System.out.print(method.getReturnType()+" ");
			System.out.print(method.getName()+" ");
			System.out.println(Arrays.toString(method.getParameterTypes()));
		}
		
		//4.getDeclaredConstructors()
		//	返回在类上声明的所有构造器
		Constructor[] constructors = cls.getDeclaredConstructors();
		System.out.println("构造器：");
		for (Constructor constructor : constructors) {
			System.out.print(constructor.getName()+" ");
			System.out.println(Arrays.toString(constructor.getParameterTypes()));
		}
		
	}

}
