package com.stormkai.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Demo2 {

	public static void main(String[] args) {
		String path = "com.stormkai.annotation.reflect.User";
		
		try {
			Class clazz = Class.forName(path);
			
			//获取类的名字
			System.out.println(clazz.getName()); //获得包名+类名 com.stormkai.annotation.reflect.User
			System.out.println(clazz.getSimpleName()); //获得类名 User
			
			//获取属性信息
			//Field[] fields = clazz.getFields(); //只获得public的field
			Field[] fields = clazz.getDeclaredFields(); //获得所有的field
			Field f = clazz.getDeclaredField("name");
			System.out.println(fields.length);
			for (Field temp : fields) {
				System.out.println("属性："+temp);
			}
			
			//获取方法信息
			Method[] methods = clazz.getDeclaredMethods();
			Method m01 = clazz.getDeclaredMethod("getName", null);
			Method m02 = clazz.getDeclaredMethod("setName", String.class);
			for (Method method : methods) {
				System.out.println("方法："+method);
			}
			
			//获取构造器信息
			Constructor[] constructors = clazz.getDeclaredConstructors();
			Constructor c = clazz.getDeclaredConstructor(long.class, String.class, int.class);
			System.out.println("获得构造器："+c);
			for (Constructor constructor : constructors) {
				System.out.println("构造器："+constructor);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		

	}

}
