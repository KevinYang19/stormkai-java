package com.stormkai.singleton.demo;
/**
 * 测试枚举实现单例模式
 * @author Admin
 *
 */
public enum SingletonDemo5 {
	
	//这个枚举元素 ，代表了Singleton的一个实例
	INSTANCE;
	
	//添加自己需要的操作
	public void singletonOperation() {
		
	}

}
