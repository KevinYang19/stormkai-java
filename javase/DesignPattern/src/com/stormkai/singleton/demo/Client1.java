package com.stormkai.singleton.demo;

public class Client1 {

	public static void main(String[] args) {
		SingletonDemo1 s1 = SingletonDemo1.getInstance();
		SingletonDemo1 s2 = SingletonDemo1.getInstance();
		System.out.println(s1 == s2);
		
		SingletonDemo2 s3 = SingletonDemo2.getInstance();
		SingletonDemo2 s4 = SingletonDemo2.getInstance();
		System.out.println(s3 == s4);
		
		SingletonDemo3 s5 = SingletonDemo3.getInstance();
		SingletonDemo3 s6 = SingletonDemo3.getInstance();
		System.out.println(s5 == s6);
		
		SingletonDemo4 s7 = SingletonDemo4.getInstance();
		SingletonDemo4 s8 = SingletonDemo4.getInstance();
		System.out.println(s7 == s8);
		
		SingletonDemo5 s9 = SingletonDemo5.INSTANCE;
		SingletonDemo5 s10 = SingletonDemo5.INSTANCE;
		System.out.println(s9 == s10);
		

	}

}
