package com.stormkai.singleton.demo;
/**
 * 测试静态内部类实现单例模式
 * @author Admin
 *
 */
public class SingletonDemo4 {
	
	private static class SingletonClassInstance {
		private static final SingletonDemo4 instance = new SingletonDemo4();
	}
	
	private SingletonDemo4() {
		
	}
	
	//方法没有同步，调用效率高
	public static SingletonDemo4 getInstance() {
		return SingletonClassInstance.instance;
	}

}
