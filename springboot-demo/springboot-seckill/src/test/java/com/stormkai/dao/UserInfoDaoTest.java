package com.stormkai.dao;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.stormkai.entity.UserInfo;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class) 
@SpringBootTest
@Slf4j
public class UserInfoDaoTest {
	
	/*@Autowired
	private UserDao userDao;

	@Test
	public void testQueryUserByUserId() {
		UserVO userVO = userDao.queryUserByUserId(1L);
		
		log.info("UserVO={}",userVO);
	}*/
	
	@Autowired
	private UserInfoDao userInfoDao;
	
	@Test
	public void testQueryUserByUserId() {
		UserInfo userInfo = userInfoDao.queryUserById(18);
		
		log.info("userInfo={}",userInfo);
	}
	
	@Test
	public void testInsertUserInfo() {
		/*UserInfo userInfo = new UserInfo();
		userInfo.setTelephone("13110472186");
		userInfo.setName("asd");
		userInfo.setAge(30);
		userInfo.setGender((short)1);
		userInfo.setRegisterMode("byPhone");
		userInfoDao.insertUserInfo(userInfo);
		
		
		log.info("i={}",userInfo);*/
		//log.info("i={}",RandomStringUtils.randomAscii(12));
	}

}
