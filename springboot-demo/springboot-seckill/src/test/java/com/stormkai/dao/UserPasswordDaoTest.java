package com.stormkai.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.stormkai.entity.UserPassword;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class) 
@SpringBootTest
@Slf4j
public class UserPasswordDaoTest {
	
	@Autowired
	private UserPasswordDao userPasswordDao;

	@Test
	public void testQueryUserPasswordById() {
		UserPassword userPassword = userPasswordDao.queryUserPasswordById(18);
		log.info("userPassword={}",userPassword);
	}

}
