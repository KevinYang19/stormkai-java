package com.stormkai.listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@WebListener
public class SeckillHttpSessionListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		log.info("===创建session===");
		System.out.println("===创建session===");
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		log.info("===销毁session===");
		System.out.println("===销毁session===");
	}

}
