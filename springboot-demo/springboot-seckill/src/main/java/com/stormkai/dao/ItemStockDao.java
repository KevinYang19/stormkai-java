package com.stormkai.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.stormkai.entity.ItemStock;

public interface ItemStockDao {
	
	@Select("select * from item_stock where item_id=#{itemId}")
	ItemStock queryItemStockByItemId(int itemId);
	
	@Insert("insert into item_stock(stock,item_id) values(#{stock}, #{itemId}) ")
	@Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
	void insertItemStock(ItemStock itemStock);
	
	@Update("update item_stock set stock = stock - #{amount} where item_id = #{itemId} and stock >= #{amount}")
	int decreaseStock(@Param("itemId") Integer itemId, @Param("amount") Integer amount);

}
