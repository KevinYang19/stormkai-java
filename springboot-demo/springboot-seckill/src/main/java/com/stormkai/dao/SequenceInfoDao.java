package com.stormkai.dao;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.stormkai.entity.SequenceInfo;

public interface SequenceInfoDao {
	
	@Select("select * from sequence_info where name = #{name} for update")
	SequenceInfo querySequenceInfoByName(String name);
	
	@Update("update sequence_info set current_value = #{currentValue}, step = #{step} where name = #{name}")
	int updateSequenceInfo(SequenceInfo sequenceInfo);

}
