package com.stormkai.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import com.stormkai.entity.UserInfo;

public interface UserInfoDao {
	
	@Select("select * from user_info where id=#{id}")
	UserInfo queryUserById(int id);
	
	@Select("select * from user_info where telephone=#{telephone}")
	UserInfo queryUserByTelephone(String telephone);
	
	@Insert("insert into user_info(name,age,gender,telephone,register_mode) values(#{name}, #{age}, #{gender}, #{telephone}, #{registerMode}) ")
	@Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
	//@SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = int.class)
	void insertUserInfo(UserInfo userInfo);

}
