package com.stormkai.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import com.stormkai.entity.UserPassword;

public interface UserPasswordDao {
	
	@Select("select * from user_password where id=#{id}")
	UserPassword queryUserPasswordById(int id);
	
	@Select("select * from user_password where user_id=#{userId}")
	UserPassword queryUserPasswordByUserId(int userId);
	
	@Insert("insert into user_password(encrpt_password,user_id) values(#{encrptPassword}, #{userId}) ")
	//@Options(useGeneratedKeys=true, keyProperty="userId", keyColumn="id")
	int insertUserPassword(UserPassword userPassword);

}
