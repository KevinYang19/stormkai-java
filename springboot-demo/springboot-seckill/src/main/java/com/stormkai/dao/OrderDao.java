package com.stormkai.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;

import com.stormkai.entity.OrderInfo;

public interface OrderDao {
	
	@Insert("insert into order_info(id,user_id,item_id,item_price,amount,order_price,promo_id) values(#{id}, #{userId}, #{itemId}, #{itemPrice}, #{amount}, #{orderPrice}, #{promoId}) ")
	@Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
	void insertOrderInfo(OrderInfo orderInfo);

}
