package com.stormkai.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.stormkai.entity.Item;

public interface ItemDao {
	
	@Select("select * from item where id=#{id}")
	Item queryItemById(int id);
	
	@Insert("insert into item(title,price,description,img_url) values(#{title}, #{price}, #{description}, #{imgUrl}) ")
	@Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
	void insertItem(Item item);
	
	@Select("select * from item order by sales desc")
	List<Item> queryItem();
	
	@Update("update item set sales = sales + #{amount} where id = #{id}")
	int increaseSales(@Param("id") Integer id, @Param("amount") Integer amount);
}
