package com.stormkai.dao;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.stormkai.entity.Promo;

public interface PromoDao {
	
	@Select("select * from promo where item_id=#{itemId}")
	Promo queryPromoByItemId(int itemId);
	
	@Update("update promo set promo_name=#{promoName},start_time=#{startTime},"
			+ "end_time=#{endTime},item_id=#{itemId}, promo_item_price=#{promoItemPrice}"
			+ " where id=#{id}")
	int updateById(Promo promo);

}
