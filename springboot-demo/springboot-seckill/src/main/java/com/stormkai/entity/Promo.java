package com.stormkai.entity;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

@Data
public class Promo {
	
	private Integer id;
	
	private String promoName;
	
	private Date startTime;
	
	private Date endTime;
	
	private Integer itemId;
	
	private BigDecimal promoItemPrice;
}
