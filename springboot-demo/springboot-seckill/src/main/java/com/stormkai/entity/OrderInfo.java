package com.stormkai.entity;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class OrderInfo {
	//订单号
	private String id;
	
	private Integer userId;
	
	private Integer itemId;
	
	//购买时价格
	private BigDecimal itemPrice;
	
	private Integer amount;
	
	private BigDecimal orderPrice;
	
	//秒杀活动ID, 若为0则表示没有参加任何秒杀活动
	private Integer promoId;

}
