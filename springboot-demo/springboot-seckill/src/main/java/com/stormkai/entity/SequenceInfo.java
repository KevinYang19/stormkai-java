package com.stormkai.entity;

import lombok.Data;

@Data
public class SequenceInfo {
	
	private String name;
	
	private Integer currentValue;
	
	private Integer step;

}
