package com.stormkai.entity;

import lombok.Data;

@Data
public class ItemStock {
	
	private Integer id;
	
	private Integer stock;
	
	private Integer itemId;

}
