package com.stormkai.entity;

import lombok.Data;

@Data
public class UserInfo {
	private Integer id;
	private String name;
	private short gender;
	private Integer age;
	private String telephone;
	private String registerMode;
	private String thirdPartyId;

}
