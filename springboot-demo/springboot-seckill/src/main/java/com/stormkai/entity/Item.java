package com.stormkai.entity;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class Item {
	
	private Integer id;
	
	private String title;
	
	private BigDecimal price;
	
	private String description;
	
	private Integer sales;
	
	private String imgUrl;

}
