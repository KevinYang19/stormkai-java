package com.stormkai.error;

public enum EnumBusinessError implements CommonError {

    //通用错误类型
    PARAMETER_VALIDATION_ERROR(10001,"参数不合法"),
    UNKONWN_ERROR(10002,"未知错误") ,

    //20000开头的为用户相关信息错误的错误代码
    USER_NOT_EXIST(20001,"用户不存在"),
    USER_LOGIN_FAIL(20002,"用户手机号或密码不正确"),
    USER_AUTHENTICATION_FAIL(20003,"用户未登录"),
    USER_AUTH_CODE_FAIL(20004,"验证码不正确"),

    //30000开头交易信息错误
    STOCK_NOT_ENOUGH(30001,"库存不足"),
    ;



    private int errCode;
    private String errMsg;

    EnumBusinessError(int errorCode, String errorMsg) {
        this.errCode = errorCode;
        this.errMsg = errorMsg;
    }


    @Override
    public int getErrorCode() {
        return this.errCode;
    }

    @Override
    public String getErrorMsg() {
        return this.errMsg;
    }

    @Override
    public EnumBusinessError setErrorMsg(String errorMsg) {
    this.errMsg =errorMsg;
    return  this;
    }
}
