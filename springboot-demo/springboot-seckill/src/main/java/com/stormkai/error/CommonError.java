package com.stormkai.error;

public interface CommonError {
    String STATUS_FAILED = "fail";
    String STATUS_SUCCESS = "success";
    int getErrorCode();
    String getErrorMsg();
    EnumBusinessError setErrorMsg(String errorMsg);
}
