package com.stormkai.service.model;

import java.math.BigDecimal;

import org.joda.time.DateTime;

import lombok.Data;

@Data
public class PromoModel {
	
	private Integer id;
    //活动名称
    private String promoName;

    //活动开始时间
    private DateTime startTime;

    //结束时间
    private DateTime endTime;

    //秒杀活动的适用商品
    private Integer itemId;

    //秒杀活动商品的价格
    private BigDecimal promoItemPrice;

    //秒杀活动状态，0表示没有，1表示还没有开始，2表示正在进行，3表示已经结束
    private Integer status;
}
