package com.stormkai.service;

import java.util.List;

import com.stormkai.error.BusinessException;
import com.stormkai.service.model.ItemModel;

public interface ItemService {
	
	//商品详情浏览
    ItemModel getItemById(Integer id);

    //创建商品
    ItemModel create(ItemModel itemModel) throws BusinessException;

    //商品列表浏览
    List<ItemModel> listItem();


    //减库存

    /**
     * @param itemId 商品ID
     * @param count 商品数量
     * @return 库存是充足
     * @throws BusinessException
     */
    boolean decreaseStock(Integer itemId,Integer amount) throws BusinessException;

    boolean increaseSales(Integer itemId,Integer amount) throws BusinessException;

}
