package com.stormkai.service.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
//用户下单的交易模型
public class OrderModel {
	
	//单号
    private String id;

    //用户id
    private Integer userId;

    //商品id
    private Integer itemId;

   //购买商品的单价，若promoId非空则是秒杀商品的价格
    private BigDecimal itemPrice;

    //购买数量
    private Integer amount;

    //购买金额，若promoId非空则是秒杀商品的总价格
    private BigDecimal orderPrice;

    //若非空则是秒杀商品下单
    private Integer promoId;

}
