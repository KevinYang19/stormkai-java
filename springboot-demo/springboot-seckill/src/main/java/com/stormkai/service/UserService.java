package com.stormkai.service;

import com.stormkai.controller.vo.UserVO;
import com.stormkai.error.BusinessException;
import com.stormkai.service.model.UserModel;

public interface UserService {
	
	UserModel getUserById(int id) throws BusinessException;
	
	void register(UserModel userModel) throws BusinessException;
	
	/**
	 * 
	 * @param telPhone 用户注册的手机
	 * @param encryptPassword 用户密码
	 * @return
	 * @throws BusinessException
	 */
	UserModel validateLogin(String telephone, String password) throws BusinessException;

}
