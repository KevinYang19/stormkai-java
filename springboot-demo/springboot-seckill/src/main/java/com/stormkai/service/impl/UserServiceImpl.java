package com.stormkai.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stormkai.controller.vo.UserVO;
import com.stormkai.dao.UserInfoDao;
import com.stormkai.dao.UserPasswordDao;
import com.stormkai.entity.UserInfo;
import com.stormkai.entity.UserPassword;
import com.stormkai.error.BusinessException;
import com.stormkai.error.EnumBusinessError;
import com.stormkai.service.UserService;
import com.stormkai.service.model.UserModel;
import com.stormkai.validator.ValidationResult;
import com.stormkai.validator.ValidatorImpl;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserInfoDao userInfoDao;
	
	@Autowired
	private UserPasswordDao userPasswordDao;
	
	@Autowired
	private ValidatorImpl validator;

	@Override
	public UserModel getUserById(int id) throws BusinessException {
		UserInfo userInfo = userInfoDao.queryUserById(id);
		
		if(userInfo == null) {
			throw new BusinessException(EnumBusinessError.USER_NOT_EXIST);
		}
		
		UserPassword userPassword = userPasswordDao.queryUserPasswordById(userInfo.getId());
		
		UserModel userModel = new UserModel();
		
		/*BeanUtils.copyProperties(userInfo, userModel);
		userModel.setEncrptPassword(userPassword.getEncrptPassword());*/
		return convertFromDataObject(userInfo, userPassword);
	}

	@Override
	@Transactional
	public void register(UserModel userModel) throws BusinessException {
		if (userModel == null) {
            throw new BusinessException(EnumBusinessError.PARAMETER_VALIDATION_ERROR);
        }
		
		ValidationResult result = validator.validate(userModel);
		if(result.isHasErrors()) {
			throw new BusinessException(EnumBusinessError.PARAMETER_VALIDATION_ERROR, result.getErrMsg());
		}
		
		UserInfo userInfo = convertFromModel(userModel);
        try {
            //int insert = userInfoDao.insertUserInfo(userInfo);
        	userInfoDao.insertUserInfo(userInfo);
        } catch (DuplicateKeyException exception) {
            throw new BusinessException(EnumBusinessError.PARAMETER_VALIDATION_ERROR,"手机号已存在");
        }
        UserPassword userPassword = convertPasswordFromModel(userModel);
        userModel.setId(userInfo.getId());
        userPassword.setUserId(userInfo.getId());
        userPasswordDao.insertUserPassword(userPassword);

	}

	@Override
	public UserModel validateLogin(String telephone, String password) throws BusinessException {
		// 通过用户的手机获取用户信息
		UserInfo userInfo = userInfoDao.queryUserByTelephone(telephone);
		if(userInfo == null) {
			throw new BusinessException(EnumBusinessError.USER_LOGIN_FAIL);
		}
		UserPassword userPassword = userPasswordDao.queryUserPasswordByUserId(userInfo.getId());
		
		UserModel userModel = convertFromDataObject(userInfo, userPassword);
		// 比对用户信息内加密的密码是否和传输进来的密码相匹配
		
		if (!StringUtils.equals(userModel.getEncrptPassword(),password)){
            throw  new BusinessException(EnumBusinessError.USER_LOGIN_FAIL);
        }
		return null;
	}
	
	
	//组装模型
    private UserModel convertFromDataObject(UserInfo userInfo, UserPassword userPassword) {
        if (userInfo == null) {
            return null;
        }

        UserModel userModel = new UserModel();
        BeanUtils.copyProperties(userInfo, userModel);
        if (userPassword != null) {
            userModel.setEncrptPassword(userPassword.getEncrptPassword());
        }
        return userModel;
    }
    
    private UserPassword convertPasswordFromModel(UserModel userModel) {
        if (userModel == null) {
            return null;
        }
        UserPassword userPassword = new UserPassword();
        userPassword.setEncrptPassword(userModel.getEncrptPassword());
        return userPassword;
    }

    private UserInfo convertFromModel(UserModel userModel) {
        if (userModel == null) {
            return null;
        }
        UserInfo userInfo = new UserInfo();
        BeanUtils.copyProperties(userModel, userInfo);
        return userInfo;
    }

}
