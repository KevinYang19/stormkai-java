package com.stormkai.service;

import com.stormkai.service.model.PromoModel;

public interface PromoService {
	
	PromoModel getPromoByItemId(Integer itemId);

}
