package com.stormkai.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stormkai.dao.ItemDao;
import com.stormkai.dao.ItemStockDao;
import com.stormkai.entity.Item;
import com.stormkai.entity.ItemStock;
import com.stormkai.error.BusinessException;
import com.stormkai.error.EnumBusinessError;
import com.stormkai.service.ItemService;
import com.stormkai.service.PromoService;
import com.stormkai.service.model.ItemModel;
import com.stormkai.service.model.PromoModel;
import com.stormkai.validator.ValidationResult;
import com.stormkai.validator.ValidatorImpl;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ItemServiceImpl implements ItemService {
	
	@Autowired
	private ItemDao itemDao;
	
	@Autowired
	private ItemStockDao itemStockDao;
	
	@Autowired
	private ValidatorImpl validator;
	
	@Autowired
	private PromoService promoService;
	
	

	@Override
	public ItemModel getItemById(Integer id) {
		if(id == null || id < 0) {
			return null;
		}
		Item item = itemDao.queryItemById(id);
		
		if(item == null) {
			return null;
		}
		
		ItemStock itemStock = itemStockDao.queryItemStockByItemId(item.getId());
		
		ItemModel itemModel = convertFromDataObject(item, itemStock);
		
		//获取活动商品信息
		PromoModel promoModel = promoService.getPromoByItemId(item.getId());
		log.info("item.getId()={},promoModel={}",item.getId(),promoModel);
		if(promoModel != null && promoModel.getStatus().intValue()!=3) {
			itemModel.setPromoModel(promoModel);
		}
		
		return itemModel;
	}

	@Override
	@Transactional
	public ItemModel create(ItemModel itemModel) throws BusinessException {
		// 检验入参
		if(itemModel == null) {
			return null;
		}
		ValidationResult validationResult = validator.validate(itemModel);
		if(validationResult.isHasErrors()) {
			throw new BusinessException(EnumBusinessError.PARAMETER_VALIDATION_ERROR,validationResult.getErrMsg());
		}
		
		Item item = convertItemFromItemModel(itemModel);
		
		itemDao.insertItem(item);
		itemModel.setId(item.getId());
		
		ItemStock itemStock = convertItemStockFromItemModel(itemModel);
		itemStockDao.insertItemStock(itemStock);;
		return this.getItemById(item.getId());
	}

	@Override
	public List<ItemModel> listItem() {
		List<Item> itemList = itemDao.queryItem();
		
		return itemList.stream().map(item -> {
			ItemStock itemStock = itemStockDao.queryItemStockByItemId(item.getId());
			return convertFromDataObject(item, itemStock);
		}).collect(Collectors.toList());
	}

	@Override
	@Transactional
	public boolean decreaseStock(Integer itemId, Integer amount) throws BusinessException {
		int affectedRow = itemStockDao.decreaseStock(itemId, amount);
		if(affectedRow > 0) {
			//更新库存成功
			return true;
		}else {
			//更新库存失败
			return false;
		}
		
	}

	@Override
	@Transactional
	public boolean increaseSales(Integer itemId, Integer amount) throws BusinessException {
		itemDao.increaseSales(itemId, amount);
		return false;
	}
	
	
	//不要把convert操作放到接口中，这样的话，不好调service
    private ItemModel convertFromDataObject(Item item, ItemStock itemStock){
        if (item == null) {
            return null;
        }
        ItemModel itemModel = new ItemModel();
        BeanUtils.copyProperties(item,itemModel);
        itemModel.setStock(itemStock.getStock());
        PromoModel promoModel = promoService.getPromoByItemId(item.getId());
        itemModel.setPromoModel(promoModel);
        return  itemModel;
    }

     private Item convertItemFromItemModel(ItemModel itemModel){

        if (itemModel == null) {
            return null;
        }

        Item item = new Item();
        BeanUtils.copyProperties(itemModel,item);
        return  item;
    }

     private ItemStock convertItemStockFromItemModel(ItemModel itemModel){
        if (itemModel == null) {
            return null;
        }
        ItemStock itemStock = new ItemStock();
        itemStock.setItemId(itemModel.getId());
        itemStock.setStock(itemModel.getStock());
        return itemStock;
     }  

}
