package com.stormkai.service.model;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;
@Data
public class ItemModel {
	
	private Integer id;
	
	//商品名
	@NotBlank(message = "商品必须有名称")
	private String title;
	
	//商品价格
	@NotNull(message = "商品价格不能为0")
    @Min(value = 0,message = "商品价格必须大于0")
	private BigDecimal price;
	
	//库存
	@NotNull(message = "必须填写商品库存")
	private Integer stock;
	
	//商品描述
	@NotBlank(message = "商品必须有描述信息")
	private String description;
	
	//商品销量
	private Integer sales;
	
	//商品图片URL
	@NotBlank(message = "商品要有图片")
	private String imgUrl;
	
	//如果有秒杀活动，该字段不为空
    private PromoModel promoModel;
	
	

}
