package com.stormkai.service.impl;

import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stormkai.dao.PromoDao;
import com.stormkai.entity.Promo;
import com.stormkai.service.PromoService;
import com.stormkai.service.model.PromoModel;
@Service
public class PromoServiceImpl implements PromoService {
	
	@Autowired
	private PromoDao promoDao;

	@Override
	public PromoModel getPromoByItemId(Integer itemId) {
		Promo promo = promoDao.queryPromoByItemId(itemId);
		
		PromoModel promoModel = convertFromDataObject(promo);
		if(promoModel == null) {
			return null;
		}
		
		//判断当前时间是否秒杀活动即将开始或正在进行
		DateTime now = new DateTime();
		if(promoModel.getStartTime().isAfterNow()) {
			promoModel.setStatus(1);
		}else if(promoModel.getEndTime().isAfterNow()) {
			promoModel.setStatus(3);
		}else {
			promoModel.setStatus(2);
		}
		return promoModel;
	}
	
	
	private PromoModel convertFromDataObject(Promo promo) {
		if(promo == null) {
			return null;
		}
		PromoModel promoModel = new PromoModel();
		BeanUtils.copyProperties(promo, promoModel);
		promoModel.setStartTime(new DateTime(promo.getStartTime()));
		promoModel.setEndTime(new DateTime(promo.getEndTime()));
		return promoModel;
	}

}
