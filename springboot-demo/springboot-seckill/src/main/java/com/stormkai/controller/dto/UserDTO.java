package com.stormkai.controller.dto;

import lombok.Data;

@Data
public class UserDTO {
	private String telephone;
	private String authCode;
	private String name;
	private short gender;
	private int age;
	private String password;
	

}
