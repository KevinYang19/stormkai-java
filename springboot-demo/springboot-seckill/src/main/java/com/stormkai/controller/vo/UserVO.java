package com.stormkai.controller.vo;



import lombok.Data;

@Data
public class UserVO {
	
	private int id;
	private String name;
	private short gender;
	private int age;
	private String telephone;

}
