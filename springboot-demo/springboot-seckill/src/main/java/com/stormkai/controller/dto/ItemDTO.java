package com.stormkai.controller.dto;

import java.math.BigDecimal;

import lombok.Data;
@Data
public class ItemDTO {
	
	private String title;
	
	private String description;
	
	private BigDecimal price;
	
	private Integer stock;
	
	private String imgUrl;

}
