package com.stormkai.controller;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.stormkai.controller.dto.ItemDTO;
import com.stormkai.controller.vo.ItemVO;
import com.stormkai.entity.Item;
import com.stormkai.error.BusinessException;
import com.stormkai.response.CommonReturnType;
import com.stormkai.service.ItemService;
import com.stormkai.service.model.ItemModel;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/item")
@CrossOrigin(origins= {"*"}, allowCredentials="true")
@Slf4j
public class ItemController extends BaseController {
	
	@Autowired
	private ItemService itemService;
	
	@PostMapping(value="/create",consumes= {"application/x-www-form-urlencoded"})
	public CommonReturnType create(ItemDTO itemDTO) throws BusinessException {
		
		ItemModel itemModel = new ItemModel();
		BeanUtils.copyProperties(itemDTO, itemModel);
		itemModel = itemService.create(itemModel);
		
		return CommonReturnType.create(convertVOFromModel(itemModel));
	}
	
	/**
	 * 商品详情页面浏览
	 * @return
	 */
	@GetMapping(value="/list")
	public CommonReturnType list() {
		List<ItemModel> itemModelList = itemService.listItem();//.stream().map(this :: convertVOFromModel).collect(Collectors.toList());
		
		//使用stream api将list内的itemModel转化为ItemVO
		List<ItemVO> itemVOList = itemModelList.stream().map(itemModel -> {
			ItemVO itemVO = this.convertVOFromModel(itemModel);
			return itemVO;
		}).collect(Collectors.toList());
		
		return CommonReturnType.create(itemVOList);
	}
	
	/**
	 * 商品列表页面浏览
	 * @param id
	 * @return
	 */
	@GetMapping(value="/get")
	public CommonReturnType get(@RequestParam(name = "id") Integer id) {
		ItemModel itemModel = itemService.getItemById(id);
		log.info("itemModel={}",itemModel);
		ItemVO itemVO = convertVOFromModel(itemModel);
		log.info("itemVO={}",itemVO);
		return CommonReturnType.create(itemVO);
	}
	
	private ItemVO convertVOFromModel(ItemModel itemModel) {
		if(itemModel == null) {
			return null;
		}
		
		ItemVO itemVO = new ItemVO();
		BeanUtils.copyProperties(itemModel, itemVO);
		if(itemModel.getPromoModel() != null) {
			//有正在进行或即将进行的秒杀活动
			itemVO.setPromoStatus(itemModel.getPromoModel().getStatus());
			itemVO.setPromoId(itemModel.getPromoModel().getId());
			//itemVO.setPromoStartTime(itemModel.getPromoModel().getStartTime().toString(DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")));
			itemVO.setPromoStartTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(itemModel.getPromoModel().getStartTime()));
			itemVO.setPromoItemPrice(itemModel.getPromoModel().getPromoItemPrice());
			
		}else {
			itemVO.setPromoStatus(0);
		}
		return itemVO;
	}

}
