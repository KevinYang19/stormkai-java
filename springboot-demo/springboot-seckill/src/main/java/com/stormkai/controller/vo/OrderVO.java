package com.stormkai.controller.vo;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class OrderVO {
	//单号
    private String id;

    //用户id
    private Integer userId;

    //商品id
    private Integer itemId;

    //购买时的价格
    private BigDecimal itemPrice;

    //购买数量
    private Integer amount;

    //购买金额
    private BigDecimal total;

}
