package com.stormkai.controller.vo;

import java.math.BigDecimal;

import org.joda.time.DateTime;

import lombok.Data;

@Data
public class ItemVO {
	
	private Integer id;
	
	//商品名
	private String title;
	
	//商品价格
	private BigDecimal price;
	
	//商品库存
	private Integer stock;
	
	//商品描述
	private  String description;
	
	//商品销量
	private Integer sales;
	
	//商品图片URL
	private String imgUrl;
	
	//秒杀活动状态，0表示没有，1表示还没有开始，2表示正在进行，3表示已经结束
	private Integer promoStatus;
	
	//活动名称
	private String promoName;
	
	//活动开始时间
	private String promoStartTime;
	
	//结束时间
	private String promoEndTime;
	
	//活动id
	private Integer promoId;
	
	//秒杀活动商品的价格
	private BigDecimal promoItemPrice;

}
