package com.stormkai;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@MapperScan("com.stormkai.dao")
@ServletComponentScan
public class SpringbootSeckillApplication {

	public static void main(String[] args) {
		//SpringApplication.run(SpringbootSeckillApplication.class, args);
		SpringApplicationBuilder builder = new SpringApplicationBuilder(SpringbootSeckillApplication.class);
		builder.bannerMode(Banner.Mode.OFF).run(args);
	}

}
