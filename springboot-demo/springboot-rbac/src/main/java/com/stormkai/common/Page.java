package com.stormkai.common;

import java.util.List;

import lombok.Data;

@Data
public class Page<T> {
	private List<T> datas;
	private int pageno;
	private int totalno;
	private int totalsize;
}
