package com.stormkai.common;

import lombok.Data;

@Data
public class AJAXResult {
	private boolean success;
	private Object data;

}
