package com.stormkai;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@MapperScan("com.stormkai.dao")
public class SpringbootRbacApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootRbacApplication.class, args);
	}

}
