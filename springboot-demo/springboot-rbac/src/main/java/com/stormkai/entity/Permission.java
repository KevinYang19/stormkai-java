package com.stormkai.entity;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Permission {
	private int id;
	private String name;
	private String url;
	private int pid;
	private boolean open = true;
	private boolean checked = false;
	private String icon;
	private List<Permission> children = new ArrayList<Permission>();

}
