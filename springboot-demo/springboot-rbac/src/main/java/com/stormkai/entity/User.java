package com.stormkai.entity;

import java.util.Date;

import lombok.Data;

@Data
public class User {
	
	private int id;
	
	private String userName;
	
	private String loginName;
	
	private String phoneNumber;
	
	private char sex;
	
	private String email;
	
	private String password;
	
	private String createTime;

	public User(String loginName, String password) {
		super();
		this.loginName = loginName;
		this.password = password;
	}
	
	

}
