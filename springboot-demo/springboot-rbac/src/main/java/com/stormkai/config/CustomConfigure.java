package com.stormkai.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.stormkai.interceptor.AuthInterceptor;
import com.stormkai.interceptor.LoginInterceptor;

@Configuration
public class CustomConfigure implements WebMvcConfigurer {

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		//WebMvcConfigurer.super.addInterceptors(registry);
		List<String> list = new ArrayList<>();
		list.add("/login");
		list.add("/doAJAXLogin");
		list.add("/bootstrap/**");
		list.add("/css/**");
		list.add("/fonts/**");
		list.add("/img/**");
		list.add("/jquery/**");
		list.add("/layer/**");
		list.add("/script/**");
		list.add("/ztree/**");
		registry.addInterceptor(new LoginInterceptor()).excludePathPatterns(list);
		
		
		//registry.addInterceptor(new AuthInterceptor()).excludePathPatterns(list);
		
		
	}

}
