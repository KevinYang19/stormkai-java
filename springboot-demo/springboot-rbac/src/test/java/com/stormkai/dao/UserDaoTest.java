package com.stormkai.dao;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.stormkai.entity.User;

import lombok.extern.slf4j.Slf4j;
@RunWith(SpringRunner.class) 
@SpringBootTest
@Slf4j
public class UserDaoTest {
	
	@Autowired
	private UserDao userDao;

	@Test
	public void testQueryForLogin() {
		User user = new User("zhangsan","zhangsan");
		List<User> list = userDao.queryAll();
		log.info(list.toString());
		Assert.assertNotNull(list.size());
	}

}
