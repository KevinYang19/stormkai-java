package com.stormkai;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.StorageClient;
import org.csource.fastdfs.StorageServer;
import org.csource.fastdfs.TrackerClient;
import org.csource.fastdfs.TrackerServer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

import com.stormkai.pojo.FastDFSFile;
import com.stormkai.utils.FastDFSClient;

import lombok.extern.slf4j.Slf4j;


@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class SpringbootFastdfsApplicationTests {
	
	@Test
	public void testUpload() throws Exception {
		// 1、初始化全局配置。加载一个配置文件。
		String filePath = new ClassPathResource("fdfs_client.conf").getFile().getAbsolutePath();
		ClientGlobal.init(filePath);
		log.info("filePath={}",filePath);
		
		// 2、创建一个TrackerClient对象。
		TrackerClient trackerClient = new TrackerClient();
		// 3、创建一个TrackerServer对象。
		TrackerServer trackerServer = trackerClient.getConnection();
		// 4、声明一个StorageServer对象，null。
		StorageServer storageServer = null;
		// 5、获得StorageClient对象。
		StorageClient storageClient = new StorageClient(trackerServer, storageServer);
		// 6、直接调用StorageClient对象方法上传文件即可。
		String[] strings = storageClient.upload_file("D:\\image\\a1.jpg", "jpg", null);
		for (String string : strings) {
			System.out.println(string);
		}
		
		
	}
	
	@Test
	public void testDeleteFile() throws Exception {
		// 1、初始化全局配置。加载一个配置文件。
		String filePath = new ClassPathResource("fdfs_client.conf").getFile().getAbsolutePath();
		ClientGlobal.init(filePath);
		log.info("filePath={}",filePath);
		
		// 2、创建一个TrackerClient对象。
		TrackerClient trackerClient = new TrackerClient();
		// 3、创建一个TrackerServer对象。
		TrackerServer trackerServer = trackerClient.getConnection();
		// 4、声明一个StorageServer对象，null。
		StorageServer storageServer = null;
		// 5、获得StorageClient对象。
		StorageClient storageClient = new StorageClient(trackerServer, storageServer);
		// 6、直接调用StorageClient对象方法上传文件即可。
		//返回值0代表删除成功
		int result = storageClient.delete_file("group1", "M00/00/00/wKgAaFyM3fSANCgXAABMnNm0e54098.jpg");
		System.out.println("result="+result);
		
	}
	
	@Test
	public void testFastDFSClient() throws Exception {
		
		//定义文件返回值
		String[] fileAbsolutePath={};
		
		//模拟上传文件过来的文件
		MockMultipartFile mockMultipartFile    = new MockMultipartFile("D:\\image\\a2.jpg", new FileInputStream(new File("D:\\image\\a2.jpg")));
		
	    String fileName=mockMultipartFile.getName();
		
		String ext = fileName.substring(fileName.lastIndexOf(".") + 1);
	    byte[] file_buff = null;
	    InputStream inputStream=mockMultipartFile.getInputStream();
	    if(inputStream!=null){
	        int len1 = inputStream.available();
	        file_buff = new byte[len1];
	        inputStream.read(file_buff);
	    }
	    inputStream.close();
	    
	    FastDFSFile file = new FastDFSFile(fileName, file_buff, ext);
	    
	    //上传文件
	    fileAbsolutePath = FastDFSClient.upload(file);
	    
	    //文件返回浏览器可以访问的路径
	    String path=FastDFSClient.getTrackerUrl()+fileAbsolutePath[0]+ "/"+fileAbsolutePath[1];
		System.out.println(path);
	}
	
	

	@Test
	public void contextLoads() {
	}

}
